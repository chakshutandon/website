FROM alpine:3.13.6
ARG DATE_MODIFIED="Fri Jan 12 10:07:28 PM EST 2024"
RUN apk update && apk add jq
CMD ["jq"]